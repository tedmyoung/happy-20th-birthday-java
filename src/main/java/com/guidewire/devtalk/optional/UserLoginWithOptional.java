package com.guidewire.devtalk.optional;

import java.util.Optional;

/**
 *
 */
public class UserLoginWithOptional {

  public void login(String username) {
    Optional<User> userResult = findUser(username);
    User user = userResult.orElse(User.GUEST);
    sendUserLoggedInEvent(user);
  }

  public Optional<User> findUser(String username) {
    return Optional.of(new User(username));
  }





  private void sendUserLoggedInEvent(User user) {

  }

  private static class User {
    public static final User GUEST = new User("guest");

    public User(String username) {

    }
  }

  public static void main(String[] args) {
    new UserLoginWithOptional().login("");
  }
}
