package com.guidewire.devtalk.streams;

/**
 *
 */
public class Monster extends Thing {

  private int _health;

  public Monster(char displayLetter, String description, Location location) {
    super(displayLetter, description, location);
  }

  public int getHealth() {
    return _health;
  }

  public void setHealth(int health) {
    _health = health;
  }

  @Override
  public String toString() {
    return "Monster:" + super.toString() + ", health: " + _health;
  }
}
