package com.guidewire.devtalk.streams;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.OptionalInt;
import java.util.Random;
import java.util.function.IntPredicate;
import java.util.stream.Collectors;

/**
 *
 */
public class Underworld {

  public static void main(String[] args) {
    Dungeon dungeon = new Dungeon();

    List<String> characterDescriptions1 = new ArrayList<>();
    for (Thing thing: dungeon.getCharacters()) {
      characterDescriptions1.add(thing.getDescription());
    }

    List<String> characterDescriptions2 = dungeon.getCharacters()
        .stream()
        .map(Thing::getDescription)
        .collect(Collectors.toList());

    characterDescriptions1.forEach(System.out::println);
    characterDescriptions2.forEach(System.out::println);

    System.out.println("--");

    dungeon.getCharacters()
           .stream()
           .peek(System.out::println)
           .flatMap(c -> c.getContents().stream())
           .peek(System.out::println)
           .map(Thing::getLocation)
           .forEach(System.out::println);

    Map<Integer, List<Item>> itemsByFloor =
        dungeon.getCharacters()
               .stream()
               .flatMap(c -> c.getContents().stream())
               .filter(item -> item.getFloor() > 0)
               .sorted()
               .collect(Collectors.groupingBy(Item::getFloor));
    System.out.println(itemsByFloor);
  }

  public static void intExample() {
    IntPredicate isEven = n -> n % 2 == 0;
    OptionalInt optionalInt = new Random().ints(10, 1, 100)
                                          .filter(isEven)
                                          .reduce((a, b) -> a + b);
    System.out.println(optionalInt);

  }

  // items in bags, grouped by characters
//
//  public void coherence() {
//    Map<Integer, Order> orders;
//
//    Map<Long, Double> salesByProduct = orders.stream()
//        .map(Map.Entry::getValue)
//        .flatMap(order -> order.getItems().stream())
//        .collect(Collectors.groupingBy(OrderItem::getProductId,
//                                       Collectors.summingDouble(OrderItem::getTotal)));
//
//  }
//
//  static Remote.BiFunction<Integer, Position, Position> stockSplit(int factor) {
//    return (key, position) -> {
//      position.setQuantity(position.getQuantity() * factor);
//      position.setPrice(position.getPrice() / factor);
//      return position;
//    };
//  }
//
//  positions.replaceAll(equal(Position::getSymbol, "AAPL"), stockSplit(7));

}
