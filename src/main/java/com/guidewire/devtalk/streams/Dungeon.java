package com.guidewire.devtalk.streams;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 */
public class Dungeon {
  private final List<Thing> _contents;
  private final Set<Character> _characters;

  public Dungeon() {
    _contents = Collections.emptyList();
    _characters = createCharacters();
  }

  private Set<Character> createCharacters() {
    return Stream.of(createCharacter(), createCharacter(), createCharacter()).collect(Collectors.toSet());
  }

  private Character createCharacter() {
    return  Character.createRandom();
  }

  public List<Thing> getContents() {
    return _contents;
  }

  public Set<Character> getCharacters() {
    return _characters;
  }
}
