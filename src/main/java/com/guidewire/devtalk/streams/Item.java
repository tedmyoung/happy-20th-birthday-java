package com.guidewire.devtalk.streams;

/**
 *
 */
public class Item extends Thing {

  private static final char[] SYMBOLS = {'!', '$', '(', '|', '~'};
  private static final String[] NAMES = {"potion", "spear", "gold", "sword", "shield"};

  public Item(char symbol, String description, Location location) {
    super(symbol, description, location);
  }

  @Override
  public String toString() {
    return "Item: " + super.toString();
  }

  public int getFloor() {
    return getLocation().getFloor();
  }

  public static Item createRandom() {
    char symbol = SYMBOLS[RANDOM.nextInt(SYMBOLS.length)];
    String description = NAMES[RANDOM.nextInt(NAMES.length)];
    int floor = RANDOM.nextInt(15) + 1;
    Coordinate coordinate = new Coordinate(RANDOM.nextInt(30), RANDOM.nextInt(30));
    return new Item(symbol, description, new Location(floor, coordinate));
  }
}
