package com.guidewire.devtalk.streams;

/**
 *
 */
public class Coordinate {
  private final int _x;
  private final int _y;

  public Coordinate(int x, int y) {
    _x = x;
    _y = y;
  }

  public int getX() {
    return _x;
  }

  public int getY() {
    return _y;
  }

  @Override
  public String toString() {
    return "(" + _x + "," + _y + ")";
  }
}
