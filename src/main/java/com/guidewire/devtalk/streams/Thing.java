package com.guidewire.devtalk.streams;

import java.util.Random;

/**
 *
 */
public class Thing {

  protected static final Random RANDOM = new Random();

  protected final char _symbol;
  protected final String _description;
  protected final Location _location;

  public Thing(char symbol, String description, Location location) {
    _symbol = symbol;
    _description = description;
    _location = location;
  }

  public String getDescription() {
    return _description;
  }

  public char getSymbol() {
    return _symbol;
  }

  public Location getLocation() {
    return _location;
  }


  @Override
  public String toString() {
    return _symbol + " (" + _description + ") at " + _location;
  }
}
