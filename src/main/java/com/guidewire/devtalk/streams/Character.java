package com.guidewire.devtalk.streams;

import java.util.Collections;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 *
 */
public class Character extends Thing {

  private static final char[] HERO_SYMBOLS = {'@', '☺', '☻', 'Θ'};
  private static final String[] FIRST_NAMES = {"Crudak", "Therlynn", "Beldak", "Sarvath"};
  private static final String[] LAST_NAMES = {"Ironhouse", "Samulkin", "Goldweaver", "Flamelaugher", "Bladeseeker"};

  private final Set<Item> _bag = new HashSet<>(); // flatMap - what all characters are holding

  public Character(char symbol, String description, Location location) {
    super(symbol, description, location);
  }

  public Set<Item> getContents() {
    return Collections.unmodifiableSet(_bag);
  }

  public void pickUp(Item item) {
    _bag.add(item);
  }

  public void drop(Item item) {
    _bag.remove(item);
  }

  public static Character createRandom() {
    char symbol = HERO_SYMBOLS[RANDOM.nextInt(HERO_SYMBOLS.length)];
    String name = FIRST_NAMES[RANDOM.nextInt(FIRST_NAMES.length)] + " " +
        LAST_NAMES[RANDOM.nextInt(LAST_NAMES.length)];
    int floor = 0;
    Coordinate coordinate = new Coordinate(RANDOM.nextInt(30), RANDOM.nextInt(30));
    Character character = new Character(symbol, name, new Location(floor, coordinate));
    character._bag.add(Item.createRandom());
    character._bag.add(Item.createRandom());
    return character;
  }


  @Override
  public String toString() {
    return "Character: " + super.toString();
  }
}
