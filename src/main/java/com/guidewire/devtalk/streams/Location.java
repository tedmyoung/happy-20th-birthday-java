package com.guidewire.devtalk.streams;

/**
 *
 */
public class Location {
  private final int _floor;
  private final Coordinate _coordinate;

  public Location(int floor, Coordinate coordinate) {
    _floor = floor;
    _coordinate = coordinate;
  }

  public Coordinate getCoordinate() {
    return _coordinate;
  }

  public int getFloor() {
    return _floor;
  }

  @Override
  public String toString() {
    return _floor + "/" + _coordinate;
  }
}
